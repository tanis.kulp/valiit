﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nimedJaVanused
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] nimed = { "Mati", "Jaan", "Joosep", "Toomas", "Kalev", "Tõnu" };
            int[] vanused = new int[nimed.Length];
            int i = 0;

            foreach (var nimi in nimed)
            {
                Console.WriteLine($"Kui vana on {nimi}?");
                while (!int.TryParse(Console.ReadLine(), out vanused[i]))
                { Console.WriteLine($"Palun kirjuta numbritega, kui vana on {nimed[i]}!");}
                Console.WriteLine($"Selge! {nimed[i]} on {vanused[i]} aastat vana :)");
                i++;
            }

            //kes on kõige vanem?
            int vanim = 0;
            for (i = 0; i < nimed.Length; i++)
            {
                if (vanused[i] > vanused[vanim])
                {
                    vanim = i;
                }
            }
            Console.WriteLine($"Kõige vanem on {nimed[vanim]}, kel eluaastaid {vanused[vanim]}.");

            //keskmine vanus
            //Console.WriteLine($"Sinu sõprade keskmine vanus on {vanused.Average()}");
            int vanusteSumma = 0;
            for (i=0;i<vanused.Length;i++)
            {
                vanusteSumma = vanusteSumma + vanused[i];
            }
            Console.WriteLine($"Sinu sõprade keskmine vanus on {vanusteSumma/vanused.Length}");

            //kelle vanus on keskmisele kõige lähemal
            int keskmineVanus = vanusteSumma / vanused.Length;
            int[] vaheKeskmisega = new int [nimed.Length];
            int väikseimVahe = vanused[vanim] - keskmineVanus;
            int väikseimVaheNr = 0;

            for (i=0; i<vanused.Length; i++)
            {
                vaheKeskmisega[i] = vanused[i] - keskmineVanus;

                if (vaheKeskmisega[i] < 0)
                { vaheKeskmisega[i] = -vaheKeskmisega[i]; }
                
                if (vaheKeskmisega[i]<väikseimVahe)
                {
                    väikseimVahe = vaheKeskmisega[i];
                    väikseimVaheNr = i;
                }
            }
            Console.WriteLine($"Kõige lähemal on keskmisele vanusele {nimed[väikseimVaheNr]}, sest ta on {vanused[väikseimVaheNr]} aastat vana.");
        }
    }
}
