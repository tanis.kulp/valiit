﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rand
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            //genereerime tabeli
            int[,] tabel = new int[8, 8];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    tabel[i, j] = r.Next(1, 100);
                }
            }

            //prindime

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Console.Write($"|{tabel[i, j]}\t");
                }
                Console.Write("\n");
            }
            //loome leitud vastuste jaoks tabeli
            int leitudArv = 0;
            int[,] leitud = new int[10,2];

            //leia arv
            int otsitav = 0;
            Console.WriteLine("Mis numbrit otsime?");

            while (!int.TryParse(Console.ReadLine(), out otsitav))
            {
                Console.Write("see pole arv, anna uus: ");
            }
            Console.WriteLine($"tubli, sinu arv on {otsitav}");

            Boolean leidsin = false;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (tabel[i, j] == otsitav)
                    {
                        leitud[leitudArv, 0] = i;
                        leitud[leitudArv, 1] = j;
                        leitudArv++;
                        leidsin = true;
                    }
                    
                }
            }
            if (leidsin == true)
            {
                Console.WriteLine($"Leidsin! Arvu {otsitav} esineb tabelis {leitudArv} korda ja need asuvad:");
                for (int i = 0; i < leitudArv; i++)
                {
                    Console.WriteLine($"reas nr {leitud[i,0]} ja veerus nr {leitud[i,1]}");
                }
            }
            else
            { Console.WriteLine("Sellist ei ole!"); }
        }
    }
}
