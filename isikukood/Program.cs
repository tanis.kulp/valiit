﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isikukood
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Sisesta oma isikukood: ");
            long isikukood = 0L;

        Sisesta:
            while (!long.TryParse(Console.ReadLine(), out isikukood))
            { Console.Write("Isikukoodid koosnevad numbritest, proovi uuesti:"); }
            while (isikukood.ToString().Length != 11)
            {
                Console.Write("numbreid peaks olema kokku 11, proovi uuesti: ");
                goto Sisesta;
            }

            int esimeneNr = int.Parse((isikukood.ToString().Substring(0, 1)));
            string sünniaasta = (isikukood.ToString().Substring(1, 2));
            string sünnikuu = (isikukood.ToString().Substring(3, 2));
            string sünnipäev = (isikukood.ToString().Substring(5, 2));

            DateTime sünniKuuPäev = DateTime.Parse($"{sünnipäev}.{sünnikuu}.{(esimeneNr - 1) / 2 + 18}{sünniaasta}");

            Console.WriteLine($"Sinu isikukood on {isikukood}. " +
                $"\nOled sündinud {sünniKuuPäev.Day}.{sünniKuuPäev.Month}.{sünniKuuPäev.Year}");

            int vanus = DateTime.Now.Year - sünniKuuPäev.Year;
            if (sünniKuuPäev.AddYears(vanus) > DateTime.Now) --vanus;
            Console.WriteLine($"Oled {vanus} aastat vana!");
        }
    }
}
