﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ÕpilasteKodutöö2
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        public static void LoeÕpilased()
        {
            var õpilasedTxt = File.ReadAllLines(@"..\..\õpilased.txt");

            var õpilased = õpilasedTxt
                .Select(x => x.Split(',').Select(y => y.Trim()).ToList())
                .Select(z => new { klass = z[2], nimi = z[1], isikukood = z[0] });

            foreach (var õpilane in õpilased)
            {
                new Õpilane(õpilane.klass, õpilane.nimi, õpilane.isikukood);
            }
        }

        public static void LoeÕpetajad()
        {
            var õpetajadTxt = File.ReadAllLines(@"..\..\õpetajad.txt");

            var õpetajad = õpetajadTxt
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
                .Select(z => new
                {
                    klass = z[2],
                    nimi = z[1],
                    isikukood = z[0],
                    klassijuh = z.Length == 4 ? z[3] : ""
                });

            foreach (var õpetaja in õpetajad)
            {
                new Õpetaja(õpetaja.klass, õpetaja.nimi, õpetaja.isikukood, õpetaja.klassijuh);
            }
        }

        public static void LoeÕpilasehinded()
        {
            var hindedTxt = File.ReadAllLines(@"..\..\hinded.txt");
            var hinded = hindedTxt
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
                .Select(z => new
                {
                    õpetaja = z[0],
                    õpilane = z[1],
                    aine = z[2],
                    hinne = int.Parse(z[3])
                }).ToList();


            foreach (Õpilane i in Õpilane.ÕpilasteNimekiri)
            {
                foreach (var rida in hinded)
                    if (i.Isikukood == rida.õpilane)
                    {
                        if (i.Päevik.Keys.Contains(rida.aine))
                        {
                            i.Päevik[rida.aine].Add(rida.hinne);
                        }
                        else
                        {
                            i.Päevik.Add(rida.aine, new List<int> { rida.hinne });
                        }
                    }
            }
        }
    }
}
