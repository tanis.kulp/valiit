﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ÕpilasteKodutöö2
{
    public partial class Õpetajad : Form
    {
        public Õpetajad()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!Õpetaja.ÕpetajateNimekiri.Any()) Program.LoeÕpetajad();

            var õpetajateNimekiriNäitamiseks = Õpetaja.ÕpetajateNimekiri
                .Select(x => new { x.Nimi, x.AineMidaÕpetab }).ToArray();

            this.dataGridView1.DataSource = õpetajateNimekiriNäitamiseks;
            dataGridView1.Columns[0].HeaderText = "Nimi";
            dataGridView1.Columns[1].HeaderText = "Aine";
        }
    }
}
