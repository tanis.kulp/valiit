﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ÕpilasteKodutöö2
{
    class Klass
    {
        public List<string> Õppeained;
        public static Dictionary<string, List<Õpilane>> KlassiÕpilased = new Dictionary<string, List<Õpilane>>();
        public string Nimetus;

        public Klass(string nimetus)
        {
            Nimetus = nimetus;
            KlassiÕpilased.Add(nimetus, new List<Õpilane>());
            Õppeained = new List<string>();
        }

        public static void LisaKlassi()
        {

            foreach (Õpilane i in Õpilane.ÕpilasteNimekiri)
            {
                if (!KlassiÕpilased.Any()) { new Klass(i.KlassKusÕpib); }
                foreach (string k in Klass.KlassiÕpilased.Keys.ToList())
                {
                    if (k == i.KlassKusÕpib)
                    {
                        KlassiÕpilased[k].Add(i);
                        goto Järgmine;
                    }
                }
                new Klass(i.KlassKusÕpib);
                KlassiÕpilased[i.KlassKusÕpib].Add(i);
            Järgmine:;
            }
        }
    }
}
