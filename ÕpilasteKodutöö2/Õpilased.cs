﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ÕpilasteKodutöö2
{
    public partial class Õpilased : Form
    {
        public Õpilased()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!Õpilane.ÕpilasteNimekiri.Any()) Program.LoeÕpilased();

            var õpilasteNimekiriNäitamiseks = Õpilane.ÕpilasteNimekiri
                .Select(x => new { x.Nimi, x.KlassKusÕpib }).ToArray();

            this.dataGridView1.DataSource = õpilasteNimekiriNäitamiseks;
            dataGridView1.Columns[0].HeaderText = "Nimi";
            dataGridView1.Columns[1].HeaderText = "Klass";
        }
    }
}
