﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ÕpilasteKodutöö2
{
    public partial class Klassid : Form
    {
        public Klassid()
        {
            InitializeComponent();
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Klassid_Load(object sender, EventArgs e)
        {
            if (!Õpilane.ÕpilasteNimekiri.Any()) Program.LoeÕpilased();
            if (!Klass.KlassiÕpilased.Any()) Klass.LisaKlassi();

            this.comboBox1.DataSource = Klass.KlassiÕpilased.Keys.ToList();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!Õpilane.ÕpilasteNimekiri.Any()) Program.LoeÕpilased();

            if (Õpilane.ÕpilasteNimekiri
                    .Select(x => x.Päevik.Values).ToList().Any())
            { Program.LoeÕpilasehinded(); }

            if (!Klass.KlassiÕpilased.Any()) Klass.LisaKlassi();

            if (comboBox1.SelectedValue == null)
            {
                return;
            }
            string valitudKlass = comboBox1.SelectedValue.ToString();

            var _1BNäitamiseks = Klass.KlassiÕpilased[valitudKlass]
                .Select(x => new { x.Nimi, keskmine = x.Päevik.SelectMany(y => y.Value).DefaultIfEmpty()?.Average() ?? 0 }).ToList();

            this.dataGridView1.DataSource = _1BNäitamiseks;
            dataGridView1.Columns[0].HeaderText = "Nimi";
            dataGridView1.Columns[1].HeaderText = "Keskmine hinne";
        }
    }
}
