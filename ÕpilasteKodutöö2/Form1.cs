﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ÕpilasteKodutöö2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Õpilased c = new Õpilased();
            c.ShowDialog();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Õpetajad c = new Õpetajad();
            c.ShowDialog();
        }

        
        private void button5_Click(object sender, EventArgs e)
        {
            Klassid c = new Klassid();
            c.ShowDialog();
        }
    }
}
