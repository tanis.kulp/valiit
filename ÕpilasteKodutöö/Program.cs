﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ÕpilasteKodutöö
{
    class Program
    {
        static void Main(string[] args)
        {
            var õpilasedTxt = File.ReadAllLines(@"..\..\õpilased.txt");

            var õpilased = õpilasedTxt
                .Select(x => x.Split(',').Select(y => y.Trim()).ToList())
                .Select(z => new { klass = z[2], nimi = z[1], isikukood = z[0] });

            foreach (var õpilane in õpilased)
            {
                new Õpilane(õpilane.klass, õpilane.nimi, õpilane.isikukood);
            }


            //foreach (string rida in File.ReadAllLines(fileÕpilased))
            //{
            //    string[] tykk = rida.Split(',');
            //    new Õpilane(tykk[2].Trim(), tykk[1].Trim(), tykk[0].Trim());
            //}


            var õpetajadTxt = File.ReadAllLines(@"..\..\õpetajad.txt");

            var õpetajad = õpetajadTxt
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
                .Select(z => new
                {
                    klass = z[2],
                    nimi = z[1],
                    isikukood = z[0],
                    klassijuh = z.Length == 4 ? z[3] : ""
                });

            foreach (var õpetaja in õpetajad)
            {
                new Õpetaja(õpetaja.klass, õpetaja.nimi, õpetaja.isikukood, õpetaja.klassijuh);
            }

            //foreach (string rida in File.ReadAllLines(fileÕpetajad))
            //{
            //    string[] tykk = rida.Split(',');
            //    if(tykk.Length == 3) {new Õpetaja(tykk[2].Trim(), tykk[1].Trim(), tykk[0].Trim());}
            //    else new Õpetaja(tykk[2].Trim(), tykk[1].Trim(), tykk[0].Trim(), tykk[3].Trim());
            //}

            var hindedTxt = File.ReadAllLines(@"..\..\hinded.txt");
            var hinded = hindedTxt
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
                .Select(z => new
                {
                    õpetaja = z[0],
                    õpilane = z[1],
                    aine = z[2],
                    hinne = int.Parse(z[3])
                }).ToList();


            foreach (Õpilane i in Õpilane.ÕpilasteNimekiri)
            {
                foreach (var rida in hinded)
                    if (i.Isikukood == rida.õpilane)
                    {
                        if (i.Päevik.Keys.Contains(rida.aine))
                        {
                            i.Päevik[rida.aine].Add(rida.hinne);
                        }
                        else
                        {
                            i.Päevik.Add(rida.aine, new List<int> { rida.hinne });
                        }
                    }
                Console.WriteLine($"{i} hinded:");
                foreach (var aine in i.Päevik.Keys)
                {
                    Console.Write($"{aine}: ");
                    foreach (int hinne in i.Päevik[aine]) { Console.Write(hinne + ", "); }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }

            foreach (Õpetaja i in Õpetaja.ÕpetajateNimekiri)
            {
                foreach (var rida in hinded)
                    if (i.Isikukood == rida.õpetaja)
                    {
                        if (i.ÕpetajaPäevik.Keys.Contains(rida.õpilane))
                        {
                            i.ÕpetajaPäevik[rida.õpilane].Add(rida.hinne);
                        }
                        else
                        {
                            i.ÕpetajaPäevik.Add(rida.õpilane, new List<int> { rida.hinne });
                        }
                    }
                Console.WriteLine($"{i} pandud hinded:");
                foreach (var õpilane in i.ÕpetajaPäevik.Keys)
                {
                    Console.Write($"{Inimene.Nimekiri[õpilane].Nimi}: ");
                    foreach (int hinne in i.ÕpetajaPäevik[õpilane]) { Console.Write(hinne + ", "); }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }




            //ÕpilasteNimekiri:
            foreach (var i in Õpilane.ÕpilasteNimekiri)
            { Console.WriteLine(i.ToString()); }

            //ÕpetajateNimekiri:
            foreach (var i in Inimene.Nimekiri.Values)
                if (i is Õpetaja) { Console.WriteLine(i.ToString()); }

            //KlassiNimekiri:
            Klass.LisaKlassi();
            foreach (string k in Klass.KlassiÕpilased.Keys)
            {
                Console.WriteLine($"{k} klassi õpilased on:");
                foreach (Õpilane i in Klass.KlassiÕpilased[k].ToList())
                { Console.WriteLine(i.ToString()); }
            }

            //KlassideNimekiri:
            if (!Klass.KlassiÕpilased.Any()) { Klass.LisaKlassi(); }
            Console.WriteLine("Meie koolis on järgmised klassid:");
            foreach (string k in Klass.KlassiÕpilased.Keys) Console.WriteLine(k);

            //Õpetaja keskmine hinne:
            var q1 = Õpetaja.ÕpetajateNimekiri
                .ToDictionary(x => x.Nimi, x => x.ÕpetajaPäevik.SelectMany(y => y.Value).ToArray().Average())
                ;
            foreach (var x in q1)
            {
                Console.WriteLine(x);
                //foreach (var hinne in x.Value) Console.Write(hinne);
                //Console.WriteLine();
            }

            //Õpilase keskmine hinne
            var q2 = Õpilane.ÕpilasteNimekiri
                .ToDictionary(x => x.Nimi, x => x.Päevik.SelectMany(y => y.Value).ToArray().Average())
                ;
            foreach (var x in q2)
            {
                Console.WriteLine(x);
            }

            //Õpilase keskmine hinne mingis aines
            //var q3 = Õpilane.ÕpilasteNimekiri
            //    .ToLookup(x => x.Nimi, x => x.Päevik); //Lookupi oleks vaja, kui küsida õpilase kaupa
            foreach (var õpilane in Õpilane.ÕpilasteNimekiri)
            {
                Console.WriteLine($"{õpilane.Nimi} keskmised hinded: ");
                foreach (var õppeaine in õpilane.Päevik.Keys) { Console.Write($"{õppeaine}:\t{õpilane.Päevik[õppeaine].Average()}\n"); }
            }

            //Klassi keskmine hinne
            if (!Klass.KlassiÕpilased.Any()) { Klass.LisaKlassi(); }
            foreach (var k in Klass.KlassiÕpilased.Keys)
            {
                var klassikeskmine = Klass.KlassiÕpilased[k]
                    .SelectMany(x => x.Päevik.SelectMany(y => y.Value)).ToArray().Average();

                Console.WriteLine($"{k} keskmine hinne on {klassikeskmine}");
            }

            //Klassi keskmine hinne mingis aines

            if (!Klass.KlassiÕpilased.Any()) { Klass.LisaKlassi(); }
            double hindedSum = 0;
            double hindedCount = 0;
            foreach (var k in Klass.KlassiÕpilased.Keys)
            {
                Console.WriteLine($"{k} keskmised hinded:");
                var m = Klass.KlassiÕpilased[k]
                    .SelectMany(x => x.Päevik.Keys).Distinct().ToArray();
                foreach (var aine in m)
                {
                    foreach (var õpilane in Klass.KlassiÕpilased[k])
                    {
                        if (õpilane.Päevik.Keys.Contains(aine))
                        {
                            hindedSum = hindedSum + õpilane.Päevik[aine].Sum();
                            hindedCount = hindedCount + õpilane.Päevik[aine].Count();
                        }
                    }

                    Console.WriteLine($"{aine}:\t{hindedSum / hindedCount}");
                    hindedSum = 0;
                    hindedCount = 0;
                }

            }
            //Klassi parim õpilane
            if (!Klass.KlassiÕpilased.Any()) { Klass.LisaKlassi(); }
            foreach (var k in Klass.KlassiÕpilased.Keys)
            {
                var parim = Klass.KlassiÕpilased[k]
                    .OrderBy(x => x.Päevik.SelectMany(y => y.Value).ToArray().Average()).Last();
                Console.WriteLine($"{k} klassi parim õpilane on {parim.Nimi}");
            }
            //Klassi parim õpilane mingis aines
            if (!Klass.KlassiÕpilased.Any()) { Klass.LisaKlassi(); }
            string parimAines = "";
            double parimAinesHinne = 0;
            foreach (var k in Klass.KlassiÕpilased.Keys)
            {
                Console.WriteLine($"{k} klassi parimad õpilased:");
                var m = Klass.KlassiÕpilased[k]
                    .SelectMany(x => x.Päevik.Keys).Distinct().ToArray();
                foreach (var aine in m)
                {
                    foreach (var õpilane in Klass.KlassiÕpilased[k])
                    {
                        if (õpilane.Päevik.Keys.Contains(aine))
                        {
                            if (õpilane.Päevik[aine].Average() > parimAinesHinne)
                            {
                                parimAines = õpilane.Nimi;
                                parimAinesHinne = õpilane.Päevik[aine].Average();
                            }
                        }
                    }
                    Console.WriteLine($"{aine}:\t{parimAines}, \tkeskmise hindega {parimAinesHinne}");
                    parimAinesHinne = 0;
                }
            }

            //Kooli parim õpilane
            var kooliParim = Õpilane.ÕpilasteNimekiri
                    .OrderBy(x => x.Päevik.SelectMany(y => y.Value).ToArray().Average()).Last();
            Console.WriteLine($"Kooli parim õpilane on {kooliParim.Nimi}");

            //Kooli parim õpilane mingis aines
            string kooliParimAines = "";
            double kooliParimAinesHinne = 0;
            var n = Õpilane.ÕpilasteNimekiri
                    .SelectMany(x => x.Päevik.Keys).Distinct().ToArray();
            Console.WriteLine("Kooli parimad ainete kaupa:");
            foreach (var aine in n)
            {
                foreach (var õpilane in Õpilane.ÕpilasteNimekiri)
                {
                    if (õpilane.Päevik.Keys.Contains(aine))
                    {
                        if (õpilane.Päevik[aine].Average() > kooliParimAinesHinne)
                        {
                            kooliParimAines = õpilane.Nimi;
                            kooliParimAinesHinne = õpilane.Päevik[aine].Average();
                        }
                    }
                }
                Console.WriteLine($"{aine}:\t{kooliParimAines}, \tkeskmise hindega {kooliParimAinesHinne}");
                kooliParimAinesHinne = 0;
            }
        }
    }
}
