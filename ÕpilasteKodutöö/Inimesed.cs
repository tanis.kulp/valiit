﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ÕpilasteKodutöö
{
    class Inimene
    {
        public string Nimi { get; private set; }
        public string Isikukood { get; private set; }
        public static Dictionary<string, Inimene> Nimekiri = new Dictionary<string, Inimene>();

        public Inimene(string nimi, string isikukood)
        {
            foreach (string inimene in Nimekiri.Keys)
            {
                if (isikukood == inimene)
                {
                    Console.WriteLine($"Inimene isikukoodiga {isikukood} on juba olemas!");
                    return;
                }
            }
            Nimi = nimi;
            Isikukood = isikukood;
            Nimekiri.Add(Isikukood, this);
        }

        public override string ToString()
        {
            return $"{Nimi} ({Isikukood})"; 
        }
    }

    class Õpilane : Inimene
    {
        public string KlassKusÕpib;
        public static List<Õpilane> ÕpilasteNimekiri = new List<Õpilane>();

        public Dictionary<string,List<int>> Päevik;
        

        public Õpilane(string klass, string nimi, string isikukood) : base(nimi, isikukood)
        {
            KlassKusÕpib = klass;
            ÕpilasteNimekiri.Add(this);
            Päevik = new Dictionary<string, List<int>>(); 
        }

        public void LisaHinded(string õppeaine)
        {
            Päevik.Add(õppeaine, new List<int>());
        }


        public override string ToString()
        {
            return $"{KlassKusÕpib} õpilane {Nimi} ({Isikukood})"; 
        }

    }

    class Õpetaja:Inimene
    {
        public string AineMidaÕpetab;
        public string KlassMidaJuhatab;
        public static List<Õpetaja> ÕpetajateNimekiri = new List<Õpetaja>();
        public Dictionary<string, List<int>> ÕpetajaPäevik;

        public Õpetaja(string aine, string nimi, string isikukood, string klassijuhataja="") : base(nimi, isikukood)
        {
            AineMidaÕpetab = aine;
            KlassMidaJuhatab = klassijuhataja;
            ÕpetajateNimekiri.Add(this);
            ÕpetajaPäevik = new Dictionary<string, List<int>>();
        }
        public override string ToString()
        {
            return KlassMidaJuhatab=="" ? $"{AineMidaÕpetab} õpetaja {Nimi} ({Isikukood})" : $"{AineMidaÕpetab} õpetaja ja {KlassMidaJuhatab} klassijuhataja {Nimi} ({Isikukood})"; 
        }
    }

}
