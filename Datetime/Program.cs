﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datetime
{
    class Program
    {
        static void Main(string[] args)
        {

            switch (DateTime.Today.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    Console.WriteLine("Joo õlut!");
                    break;
                case DayOfWeek.Saturday:
                    Console.WriteLine("Küta sauna!");
                    goto case DayOfWeek.Sunday;
                case DayOfWeek.Wednesday:
                    Console.WriteLine("Mine trenni!");
                    goto default;
                default:
                    Console.WriteLine("Mine tööle!");
                    break;
            }
            Console.ReadKey();
        }
    }
}
