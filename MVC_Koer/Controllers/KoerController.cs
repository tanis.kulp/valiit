﻿using MVC_Koer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_Koer.Controllers
{
    public class KoerController : Controller
    {
        // GET: Koer
        public ActionResult Index()
        {
            return View(Koer.Koerad);
        }

        // GET: Koer/Details/5
        public ActionResult Details(int id)
        {
            return View(Koer.Find(id));
        }

        // GET: Koer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Koer/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                new Koer
                {
                    Kood = Koer.NewKood(),
                    Nimi = collection["Nimi"],
                    Tõug = collection["Tõug"],
                    Omanik = collection["Omanik"]
                }.Add();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Koer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(Koer.Find(id));
        }

        // POST: Koer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                Koer koer = Koer.Find(id);
                koer.Nimi = collection["Nimi"];
                koer.Tõug = collection["Tõug"];
                koer.Omanik = collection["Omanik"];
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Koer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(Koer.Find(id));
        }

        // POST: Koer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                Koer.Find(id).Delete();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
