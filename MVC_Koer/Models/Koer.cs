﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_Koer.Models
{
    public class Koer
    {
        static Dictionary<int, Koer> _Koerad = new Dictionary<int, Koer>();
        public static IEnumerable<Koer> Koerad => _Koerad.Values;

        [Key]public int Kood { get; set; }
        public string Nimi { get; set; }
        public string Tõug { get; set; }
        public string Omanik { get; set; }

        public void Add()
        {
            if (!_Koerad.Keys.Contains(this.Kood))
                _Koerad.Add(this.Kood, this);
        }

        public static int NewKood()
            => _Koerad.Keys.DefaultIfEmpty().Max() + 1;

        public static Koer Find(int kood)
        {
            if (_Koerad.Keys.Contains(kood)) return _Koerad[kood];
            else return null;
        }

        public void Delete()
        {
            if (_Koerad.Keys.Contains(this.Kood))
                _Koerad.Remove(this.Kood);
        }

    }
}