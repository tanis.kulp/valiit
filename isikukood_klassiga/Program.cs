﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace isikukood_klassiga
{
    class Inimesed
    {
        public string nimi;
        public DateTime sünniaeg;
        public int vanus;
        public int sugu;
    }
    class Program
    {
        static void Main(string[] args)
        {
            string failinimi = @"..\..\isikukoodid.txt";
            string[] lines = File.ReadAllLines(failinimi);

            List<Inimesed> andmed = new List<Inimesed>();
            DateTime[] sünniajad = new DateTime[lines.Length];
            string sajand = "0";
            string aasta = "0";
            string kuu = "0";
            string päev = "0";
            
            for(int i = 0; i<lines.Length; i++)
            {
                sajand = lines[i].Split(',')[1].Trim().Substring(0, 1);
                aasta = lines[i].Split(',')[1].Trim().Substring(1, 2);
                kuu = lines[i].Split(',')[1].Trim().Substring(3, 2);
                päev = lines[i].Split(',')[1].Trim().Substring(5, 2);

                andmed.Add(new Inimesed
                {
                    nimi = lines[i].Split(',')[0].Trim(),
                    sünniaeg = DateTime.Parse(($"{päev}.{kuu}.{(int.Parse(sajand) - 1) / 2 + 18}{aasta}")),
                    vanus = 0,
                    sugu = (int.Parse(sajand) % 2 == 0 ? 0:1)

                });
                andmed[i].vanus = DateTime.Now.Year - andmed[i].sünniaeg.Year;
                if (DateTime.Now < andmed[i].sünniaeg.AddYears(andmed[i].vanus)) andmed[i].vanus--; 

                Console.WriteLine($"{andmed[i].nimi}, {andmed[i].sünniaeg}, {andmed[i].vanus}, {(andmed[i].sugu == 0 ? "naine" : "mees")}");


            }

        }
    }
}
