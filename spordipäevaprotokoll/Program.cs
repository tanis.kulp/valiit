﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace spordipäevaprotokoll
{
    class Protok
    {
        public string Nimi;
        public int Distants;
        public int Aeg; //sekundites
        public double Kiirus;
    }
    class Program
    {
        static void Main(string[] args)
        {
            string failinimi = @"..\..\protokoll.txt";
            string[] lines = File.ReadAllLines(failinimi);

            Protok[] tulemused = new Protok[lines.Length - 1];

            for (int i = 0; i < lines.Length - 1; i++)
            {
                tulemused[i] = new Protok()
            
                {
                    Nimi = lines[i + 1].Split(',')[0].Trim(),
                    Distants = int.Parse(lines[i + 1].Split(',')[1].Trim()),
                    Aeg = int.Parse(lines[i + 1].Split(',')[2].Trim())
                    
                };
                tulemused[i].Kiirus = (double)tulemused[i].Distants / tulemused[i].Aeg;
            }

            int kiireimSprinter = 0;
            int parimAeg = tulemused[1].Aeg;
            int kiireimJooksja = 0;
            double suurimKiirus = tulemused[1].Kiirus;

            for (int i = 0; i < tulemused.Length; i++)
            {
                if (tulemused[i].Distants == 100)
                {
                    if (tulemused[i].Aeg < parimAeg)
                    {
                        parimAeg = tulemused[i].Aeg;
                        kiireimSprinter = i;
                    }
                }

                if (tulemused[i].Kiirus > suurimKiirus)
                {
                    suurimKiirus = tulemused[i].Kiirus;
                    kiireimJooksja = i;
                }
            }
                Console.WriteLine($"Kiireim sprinter on {tulemused[kiireimSprinter].Nimi}, tema 100m aeg on {tulemused[kiireimSprinter].Aeg}.");
                Console.WriteLine($"Kiireim jooksja on {tulemused[kiireimJooksja].Nimi}, tema kiirus distantsil {tulemused[kiireimJooksja].Distants}m oli {tulemused[kiireimJooksja].Kiirus} ja aeg {tulemused[kiireimJooksja].Aeg}.");

            
        }
    }
}
