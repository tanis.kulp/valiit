﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuleVärv
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Mis värvi on tuli? ");
            string tuleVarv = Console.ReadLine().ToLower();

            //if (tuleVarv == "green" || tuleVarv == "roheline")
            //{
            //    Console.WriteLine("Sõida!");
            //}
            //else if (tuleVarv == "yellow" || tuleVarv == "kollane")
            //{
            //    Console.WriteLine("Oota!");
            //}
            //else if (tuleVarv == "red" || tuleVarv == "punane")
            //{
            //    Console.WriteLine("Peatu!");
            //}
            //else
            //{
            //    Console.WriteLine("Valgusfoor on katki! Ole ettevaatlik!");
            //}

            switch(tuleVarv)
            {
                case "roheline":
                case "green":
                    Console.WriteLine("Sõida!");
                    break;
                case "kollane":
                case "yellow":
                    Console.WriteLine("Oota!");
                    break;
                case "punane":
                case "red":
                    Console.WriteLine("Peatu!");
                    break;
                default:
                    Console.WriteLine("Valgusfoor on katki! Ole ettevaatlik!");
                    break;
            }
            Console.ReadKey();
        }

    }
}
