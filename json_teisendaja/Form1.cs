﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace json_teisendaja
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<string[]> csv = new List<string[]>();
        string jsonfile;

        public void button1_Click(object sender, EventArgs e)
        {

            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    string[] text = File.ReadAllLines(file);
                    foreach (string rida in text)
                        csv.Add(rida.Split(',').Select (x => x.Trim()).ToArray());
                }
                catch (IOException)
                {
                    MessageBox.Show("Mingi jama...");
                }
                jsonfile = file.Split('.').First() + ".json";
                this.label1.Text = $"valitud fail: {file}";
               
            }
            
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                string[] pealkirjad = csv[0];
                var jsonTulemusPealkirjadega = new List<Dictionary<string, string>>();
                for (int i = 1; i < csv.Count(); i++)
                {
                    var sisuTulp = new Dictionary<string, string>();
                    for (int j = 0; j < pealkirjad.Length; j++)
                        sisuTulp.Add(pealkirjad[j], csv[i][j]);

                    jsonTulemusPealkirjadega.Add(sisuTulp);
                }
                string json = JsonConvert.SerializeObject(jsonTulemusPealkirjadega);

                File.WriteAllText(jsonfile, json);
            }
            else
            {
                string json = JsonConvert.SerializeObject(csv);
                File.WriteAllText(jsonfile, json);
            }

            MessageBox.Show($"Json fail on loodud:\n{jsonfile}");

        }
    }
}
